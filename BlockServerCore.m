//
//  BlockServerCore.m
//  Alef
//
//  Created by LaGrunge on 8/30/16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import "BlockServerCore.h"

@implementation BlockServerCore

- (void)requestWithNoParamsAndCallback:(RequestCallback)callback {
    [_requestManager executeRequestWithBaseURL:self.baseURL HTTPMethod:GET params:@{@"func":@"no_params"} parameterEncoding:URL headers:nil completionHandler:callback];
}

- (void)requestWithStringParameter:(NSString *)str andCallback:(RequestCallback)callback {
    [_requestManager executeRequestWithBaseURL:self.baseURL HTTPMethod:GET params:@{@"func":@"str_param", @"str_param":str} parameterEncoding:URL headers:nil completionHandler:callback];
}

- (void)requestWithImageParameter:(UIImage *)img andCallback:(RequestCallback)callback {
    [_requestManager uploadImages:@[img] withNames:@[@"image"] toURL:self.baseURL params:@{@"func":@"img"} fileName:@"image.jpeg" completionHandler:callback];
}

- (void)requestWithImageParameter:(UIImage *)img andLongParameter:(long)lng andCallback:(RequestCallback)callback {
    [_requestManager uploadImages:@[img] withNames:@[@"image"] toURL:self.baseURL params:@{@"func":@"img_and_long", @"lng":[NSNumber numberWithLong: lng]} fileName:@"image.jpeg" completionHandler:callback];
}

- (void)requestWithImageParameter:(UIImage *)img anotherImage:(UIImage *)anotherImg andCallback:(RequestCallback)callback {
    [_requestManager uploadImages:@[img, anotherImg] withNames:@[@"image", @"anotherImage"] toURL:self.baseURL params:@{@"func":@"img_and_img2"} fileName:@"image.jpeg" completionHandler:callback];
}



- (instancetype)init {
    if (self = [super init]) {
        _requestManager = [[HTTPRequestCore alloc] init];
        self.baseURL = [NSURL URLWithString: @"http://block.alef.im/api/api.php"];
    }
    return self;
}

- (void)dealloc {
    _requestManager = nil;
}

@end
