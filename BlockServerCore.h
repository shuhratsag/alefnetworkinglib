//
//  BlockServerCore.h
//  Alef
//
//  Created by LaGrunge on 8/30/16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPRequestCore.h"
@class UIImage;

@interface BlockServerCore : NSObject

{
    HTTPRequestCore *_requestManager;
}

@property (nonatomic) NSURL *baseURL;

- (void) requestWithNoParamsAndCallback: (RequestCallback)callback; // http://block.alef.im/api/api.php?func=no_params
- (void) requestWithStringParameter:(NSString*)str andCallback:(RequestCallback)callback; // http://block.alef.im/api/api.php?func=str_param&s.....
- (void) requestWithImageParameter:(UIImage*)img andCallback:(RequestCallback)callback; // POST http://block.alef.im/api/api.php (img=...)
- (void) requestWithImageParameter:(UIImage*)img andLongParameter:(long) lng andCallback:(RequestCallback)callback; // POST http://block.alef.im/api/api.php (img=..., lng=...)
- (void) requestWithImageParameter:(UIImage*)img anotherImage:(UIImage*)anotherImg andCallback:(RequestCallback)callback;

@end
