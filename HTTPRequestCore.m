//
//  HTTPRequestCore.m
//  Alef
//
//  Created by LaGrunge on 8/22/16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import "HTTPRequestCore.h"
#import <UIKit/UIKit.h>

typedef void (^RetryCallback) (void);

@interface HTTPRequestCore () <UIAlertViewDelegate>

@end

@implementation HTTPRequestCore

{
    RetryCallback _retryCallback;
    
}

- (void) executeRequestWithURLString:(NSString *)urlString method:(HTTPMethod)method parameters:(NSDictionary *)params parameterEncoding:(HTTPRequestParameterEncoding)encoding headers:(NSDictionary *)headers completionHandler:(RequestCallback)callback {
    NSURL *url = [NSURL URLWithString:urlString];
    [self executeRequestWithBaseURL:url HTTPMethod:method params:params parameterEncoding:encoding headers:headers completionHandler:callback];
}

- (void) executeRequestWithURL:(NSURL *)url completionHandler:(RequestCallback)callback {
    [self executeRequestWithBaseURL:url HTTPMethod:GET params:nil parameterEncoding:URL headers:nil completionHandler:callback];
}

-(void) executeRequestWithURL: (NSURL *)url headers: (NSDictionary *)headers completionHandler: (RequestCallback)callback {
    [self executeRequestWithBaseURL:url HTTPMethod:GET params:nil parameterEncoding:URL headers:headers completionHandler:callback];
}

- (void) executeRequestWithBaseURL:(NSURL *)baseURL HTTPMethod:(HTTPMethod)method params:(NSDictionary *)params parameterEncoding:(HTTPRequestParameterEncoding)encoding headers: (NSDictionary *) headers completionHandler:(RequestCallback)callback {
    if (baseURL) {
        NSURLRequest *request = [self requestWithParams:params baseURL:baseURL method:method encoding:encoding headers:headers];
        [self executeRequest:request completionHandler:callback];
    } else {
        NSError *error = [NSError errorWithDomain:NSCocoaErrorDomain code:1 userInfo:@{@"Error":@"Invalid URL"}];
        if (callback) {
            callback(error, nil);
        } else {
            NSLog(@"Error: Invalid URL");
        }
    }
}

// nil completionHandler causes infinite alert if error occures

- (void) executeRequest:(NSURLRequest *)request completionHandler:(RequestCallback)callback {
    NSURLSessionDataTask* dataTask = [self.defaultSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary* dict;
        if (error == nil && data != nil) {
            dict = [self dictionaryWithJSONData:data error:&error];
        }
        if (callback) {
            callback(error, dict);
        } else if (error) {
            __block HTTPRequestCore *blockSafeSelf = self;
            __block NSURLRequest *blockSafeRequest = request;
            _retryCallback = ^(void){
                [blockSafeSelf executeRequest:blockSafeRequest completionHandler:nil];
            };
            [self showNetworkAlert];
        }
    }];
    [dataTask resume];
}

#pragma mark - Upload

- (void) uploadImages:(NSArray *)images withNames:(NSArray *)names toURL:(NSURL *)url params:(NSDictionary *)params fileName:(NSString *)fileName completionHandler:(RequestCallback)callback {
    NSMutableArray *dataObjects = [NSMutableArray array];
    for (UIImage* image in images) {
        if ([image isKindOfClass:[UIImage class]]) {
            [dataObjects addObject: UIImageJPEGRepresentation(image, 1.0f)];
        }
    }
    [self executeUploadRequestWithURL:url params:params dataObjects:dataObjects withNames:names dataContentType:@"image/jpeg" fileName:fileName completionHandler:callback];
}

- (void) executeUploadRequestWithURL:(NSURL *)url params: (NSDictionary *)params dataObjects:(NSArray *)dataObjects withNames: (NSArray *)names dataContentType: (NSString *)dataContentType fileName: (NSString *)fileName completionHandler: (RequestCallback) callback {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", [self boundaryConstant]];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    NSData *body = [self multipartFormDataBodyWithDataObjects:dataObjects withNames:names fileName:fileName contentType:dataContentType andParams:params];
    NSString *bodyLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:bodyLength forHTTPHeaderField:@"Content-Length"];
    [self executeUploadRequest:request withData:body completionHandler:callback];
}

- (void) executeUploadRequest:(NSURLRequest *)request withData:(NSData *)data completionHandler:(RequestCallback)callback {
    NSURLSessionTask *task = [self.defaultSession uploadTaskWithRequest:request fromData:data completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary *dict;
        if (!error && data) {
            dict = [self dictionaryWithJSONData:data error:&error];
        }
        if (callback) {
            callback(error, dict);
        }
        
    }];
    [task resume];
}

#pragma mark - Init

- (id) init {
    if (self = [super init]) {
        _defaultSession = [NSURLSession sessionWithConfiguration: [NSURLSessionConfiguration	 defaultSessionConfiguration]];
    }
    return self;
}

#pragma mark - Helper Methods

- (NSDictionary *) dictionaryWithJSONData: (NSData *)data error: (NSError **)error {
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"SERVER RESPONSE: %@", str);
    NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:error];
    return dict;
}


- (NSURLRequest *) requestWithParams: (NSDictionary *)params baseURL: (NSURL*) baseURL method: (HTTPMethod) method encoding: (HTTPRequestParameterEncoding) encoding headers: (NSDictionary *)headers {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    switch (method) {
        case GET:
            [self configureGETRequest:request baseURL:baseURL andParams:params];
            break;
        case POST:
            [self configurePOSTRequest:request baseURL:baseURL params:params parameterEncoding:encoding];
            break;
    }
    [self setRequestHeaders:request headers:headers];
    return request;
}

- (void) configureGETRequest: (NSMutableURLRequest *)request baseURL: (NSURL *)baseURL andParams: (NSDictionary *)params {
    NSURLComponents *comps = [NSURLComponents componentsWithURL:baseURL resolvingAgainstBaseURL:YES];
    NSArray *items = [self queryItemsWithParams:params];
    comps.queryItems = items;
    [request setURL:comps.URL];
    [request setHTTPMethod:@"GET"];
}

- (void) configurePOSTRequest: (NSMutableURLRequest *)request baseURL: (NSURL *)baseURL params: (NSDictionary *)params parameterEncoding: (HTTPRequestParameterEncoding)encoding {
    [request setURL:baseURL];
    [request setHTTPMethod:@"POST"];
    NSError *error = nil;
    [self configurePOSTRequestBody:request params:params andEncoding:encoding error:&error];
}

- (void) configurePOSTRequestBody: (NSMutableURLRequest *)request params: (NSDictionary *)params andEncoding: (HTTPRequestParameterEncoding) encoding error: (NSError **) error {
    NSData *body;
    switch (encoding) {
        case URL:
             body = [self URLDataWithParams:params];
            break;
        case JSON:
            body = [self JSONDataWithParams:params error:error];
            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    }
    [request setHTTPBody:body];
}

- (void) setRequestHeaders: (NSMutableURLRequest *)request headers: (NSDictionary *)headers {
    for (id key in headers) {
        [request setValue:[NSString stringWithFormat:@"%@", headers[key]] forHTTPHeaderField:[NSString stringWithFormat:@"%@", key]];
    }
}

- (NSData *) URLDataWithParams: (NSDictionary *)params {
    NSArray *items = [self queryItemsWithParams:params];
    return [self dataWithQueryItems:items];
}

- (NSData *) dataWithQueryItems: (NSArray *) items {
    NSURLComponents *comps = [[NSURLComponents alloc] init];
    comps.queryItems = items;
    NSString *urlString = comps.URL.absoluteString;
    urlString = [urlString stringByReplacingOccurrencesOfString:@"?" withString:@""];
    return [urlString dataUsingEncoding:NSASCIIStringEncoding];
}

- (NSData *) JSONDataWithParams: (NSDictionary *)params error: (NSError **) error {
    return [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:error];
}

- (NSArray *) queryItemsWithParams: (NSDictionary *)params {
    NSMutableArray *items = [NSMutableArray array];
    for (id key in params) {
        [items addObject:[NSURLQueryItem queryItemWithName:[NSString stringWithFormat:@"%@", key] value:[NSString stringWithFormat:@"%@", params[key]]]];
    }
    return items;
}

#pragma mark - Multipart Form Data

- (NSData *) multipartFormDataBodyWithDataObjects: (NSArray *)dataObjects withNames: (NSArray *)names fileName: (NSString *)fileName contentType: (NSString *)contentType andParams: (NSDictionary *)params {
    NSMutableData *body = [NSMutableData data];
    for (id key in params) {
        [self appendFormDataWithParamName:[NSString stringWithFormat:@"%@", key] value:[[NSString stringWithFormat:@"%@", params[key]] dataUsingEncoding:NSUTF8StringEncoding] toBody:body];
    }
    NSUInteger count = 0;
    for (NSData* data in dataObjects) {
        if ([data isKindOfClass:[NSData class]] && [names count] > count) {
            [self appendFormData:data withName:[NSString stringWithFormat: @"%@", names[count]] filename:fileName andContentType:contentType toBody:body];
        }
        count++;
    }
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", [self boundaryConstant]] dataUsingEncoding:NSUTF8StringEncoding]];
    return body;
}

- (void) appendFormDataWithParamName: (NSString *)name value: (NSData *)value toBody: (NSMutableData *)body  {
    [body appendData:[[NSString stringWithFormat: @"--%@\r\n", [self boundaryConstant]] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", name] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:value];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[[NSString stringWithFormat:@"%@\r\n", value] dataUsingEncoding:NSUTF8StringEncoding]];
}

- (void) appendFormData: (NSData *)data withName: (NSString *)name filename: (NSString *)filename andContentType: (NSString *)contentType toBody: (NSMutableData *) body {
    [body appendData:[[NSString stringWithFormat: @"--%@\r\n", [self boundaryConstant]] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", name, filename] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", contentType] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:data];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
}

- (NSString *) boundaryConstant {
    return @"----------V2ymHFg03ehbqgZCaKO6jy";
}

#pragma Alert

- (void)showNetworkAlert {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Нет связи с сервером" delegate:self cancelButtonTitle:@"Повторить" otherButtonTitles:nil, nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [alertView show];
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    _retryCallback();
    _retryCallback = nil;
}	

@end
