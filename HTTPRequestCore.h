//
//  HTTPRequestCore.h
//  Alef
//
//  Created by LaGrunge on 8/22/16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^RequestCallback) (NSError* error, NSDictionary* dict);

typedef enum : NSUInteger {
    GET,
    POST,
} HTTPMethod;

typedef enum : NSUInteger {
    URL,
    JSON,
} HTTPRequestParameterEncoding;

@interface HTTPRequestCore : NSObject

@property (nonatomic) NSURLSession* defaultSession;

- (void) executeRequestWithURLString: (NSString *)urlString method: (HTTPMethod)method parameters: (NSDictionary *)params parameterEncoding: (HTTPRequestParameterEncoding)encoding headers: (NSDictionary *) headers completionHandler: (RequestCallback) callback;

- (void) executeRequestWithURL: (NSURL *)url completionHandler: (RequestCallback)callback;

- (void) executeRequestWithURL: (NSURL *)url headers: (NSDictionary *)headers completionHandler: (RequestCallback)callback;

- (void) executeRequestWithBaseURL:(NSURL *)baseURL HTTPMethod:(HTTPMethod)method params:(NSDictionary *)params parameterEncoding:(HTTPRequestParameterEncoding)encoding headers: (NSDictionary *) headers completionHandler:(RequestCallback) callback;

- (void) executeRequest: (NSURLRequest *)request completionHandler: (RequestCallback)callback;

// Upload
- (void) executeUploadRequest: (NSURLRequest *)request withData: (NSData *)data completionHandler: (RequestCallback)callback;
- (void) executeUploadRequestWithURL:(NSURL *)url params: (NSDictionary *)params dataObjects:(NSArray *)dataObjects withNames: (NSArray *)names dataContentType: (NSString *)dataContentType fileName: (NSString *)fileName completionHandler: (RequestCallback) callback;
// Array Of UIImageObjects
- (void) uploadImages: (NSArray *)images withNames: (NSArray *)names toURL: (NSURL *)url params: (NSDictionary *)params fileName: (NSString *)fileName completionHandler: (RequestCallback) callback;


@end
